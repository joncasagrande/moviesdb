package com.joncasagrande.moviesdb.modules

import com.joncasagrande.moviesdb.rest.BaseApi
import com.joncasagrande.moviesdb.rest.BaseHttp
import org.koin.dsl.module

// just declare it
val ApiModule = module {
    // provided web components
    single { BaseApi(get()) }
    single { BaseHttp()}


}