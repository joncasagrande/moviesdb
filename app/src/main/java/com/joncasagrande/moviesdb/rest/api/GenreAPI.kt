package com.joncasagrande.moviesdb.rest.api

import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface GenreAPI {

    @GET("/genre/movie/list")
    @Headers("Content-type: application/json")
    fun getGenres(@Query("api_key") key: String,
                  @Query("language") language: String)

}