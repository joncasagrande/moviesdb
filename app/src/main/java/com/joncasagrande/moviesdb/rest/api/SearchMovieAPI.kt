package com.joncasagrande.moviesdb.rest.api

import com.joncasagrande.moviesdb.model.RestDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface SearchMovieAPI {

    @GET("search/movie")
    @Headers("Content-type: application/json")
    fun getListMovies(@Query("api_key")key: String ,
                    @Query("language")language: String ,
                    @Query("query") client: String ,
                    @Query("include_adult")  signature: String): Single<RestDTO>




}