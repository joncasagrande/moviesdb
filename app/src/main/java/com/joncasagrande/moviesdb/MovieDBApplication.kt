package com.joncasagrande.moviesdb

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MovieDBApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            // Android context
            androidContext(this@MovieDBApplication)
        }
        instance = this
    }


    companion object {
        private val TAG = MovieDBApplication::class.java.simpleName

        var instance: MovieDBApplication? = null
            private set
    }
}