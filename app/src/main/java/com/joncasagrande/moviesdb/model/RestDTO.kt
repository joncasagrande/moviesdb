package com.joncasagrande.moviesdb.model

import com.google.gson.annotations.SerializedName

data class RestDTO(val page :Int,
                   @SerializedName("total_results") val totalResults : Int,
                   @SerializedName("total_pages") val totalPages : Int,
                   val results : MutableList<Movie>)