package com.joncasagrande.moviesdb.model


data class Genre(val id : Int, val name: String)
/*
 {
      "id": 28,
      "name": "Action"
    }
 */