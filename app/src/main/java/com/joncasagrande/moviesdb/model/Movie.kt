package com.joncasagrande.moviesdb.model

import com.google.gson.annotations.SerializedName

class Movie(val id: Int, val popularity : Float, val video : Boolean,
            @SerializedName("vote_count")val voteCount: Int,
            @SerializedName("vote_average")val voteAvg: Float,
            val title: String,
            @SerializedName("release_date")val releaseDate: String,
            @SerializedName("original_language")val originaLaguage: String,
            @SerializedName("original_title")val originaTitle: String,
            @SerializedName("genre_ids")val genres: List<Int>,
            @SerializedName("backdrop_path")val backdropPath: String,
            val adult: Boolean,
            val overview : String,
            @SerializedName("poster_path")val posterPath: String) {
}

/*
{
    "popularity": 9.138,
    "id": 12242,
    "video": false,
    "vote_count": 1152,
    "vote_average": 6.3,
    "title": "Mulan II",
    "release_date": "2004-11-13",
    "original_language": "en",
    "original_title": "Mulan II",
    "genre_ids": [
        16,
        35,
        10751,
        28
    ],
    "backdrop_path": "/f8BobrxGnAFObYRSN1BTIzQbEMW.jpg",
    "adult": false,
    "overview": "Fa Mulan gets the surprise of her young life when her love, Captain Li Shang asks for her hand in marriage. Before the two can have their happily ever after, the Emperor assigns them a secret mission, to escort three princesses to Chang'an, China. Mushu is determined to drive a wedge between the couple after he learns that he will lose his guardian job if Mulan marries into the Li family.",
    "poster_path": "/ebuRP7g3tJlLxeejylu4Wx4Qsjv.jpg"
}
 */